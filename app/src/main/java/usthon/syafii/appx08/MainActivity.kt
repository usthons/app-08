package usthon.syafii.appx08

import android.content.Context
import android.content.SharedPreferences
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.SeekBar
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*
import java.util.prefs.AbstractPreferences

class MainActivity : AppCompatActivity(), View.OnClickListener {

    lateinit var preferences: SharedPreferences
    val PREF_NAME = "setting"
    val Field_FONT_SIZE = "font_size"
    val FIELD_TEXT = "teks"
    val DEF_FONT_SIZE = 12
    val DEF_TEXT = "Hello World"

    val onSeek = object : SeekBar.OnSeekBarChangeListener {
        override fun onProgressChanged(seekBar: SeekBar?, progress: Int, fromUser: Boolean) {
            edText.setTextSize(progress.toFloat())
        }

        override fun onStartTrackingTouch(seekBar: SeekBar?) {

        }

        override fun onStopTrackingTouch(seekBar: SeekBar?) {

        }
    }
    
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        preferences = getSharedPreferences(PREF_NAME,Context.MODE_PRIVATE)
        edText.setText(preferences.getString(FIELD_TEXT,DEF_TEXT))
        edText.textSize = preferences.getInt(Field_FONT_SIZE,DEF_FONT_SIZE).toFloat()
        sbar.progress = preferences.getInt(Field_FONT_SIZE,DEF_FONT_SIZE)
        sbar.setOnSeekBarChangeListener(onSeek)
        btnSimpan.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        preferences = getSharedPreferences(PREF_NAME,Context.MODE_PRIVATE)
        val prefEditor = preferences.edit()
        prefEditor.putString(FIELD_TEXT,edText.text.toString())
        prefEditor.putInt(Field_FONT_SIZE,sbar.progress)
        prefEditor.commit()
        Toast.makeText(this, "Perubahan telah Di Simpan",Toast.LENGTH_SHORT).show()
    }
}
